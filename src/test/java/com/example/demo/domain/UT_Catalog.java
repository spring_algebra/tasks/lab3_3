/*
 * Algebra labs.
 */

package com.example.demo.domain;

import static org.junit.Assert.*;

import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
	
import com.example.demo.config.SpringConfig;
import com.example.demo.service.Catalog;

public class UT_Catalog {

	@Test
	public void catalogTest() {
		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
		
		// TODO: Set an active profile

		ctx.register(SpringConfig.class);
		ctx.refresh();
		
		assertTrue("spring container should not be null", ctx != null);
		
		Catalog cat = ctx.getBean(Catalog.class);
		System.out.println(cat);

		ctx.close();
	}

}
