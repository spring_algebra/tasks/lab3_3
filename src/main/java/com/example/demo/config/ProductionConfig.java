/*
 * Algebra labs.
 */
 
package com.example.demo.config;

import javax.inject.Inject;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.example.demo.persistence.ItemRepository;
import com.example.demo.persistence.RepositoryType;
import com.example.demo.persistence.StorageType;
import com.example.demo.service.Catalog;
import com.example.demo.service.CatalogImpl;

// Declare as a Spring configuration class
// TODO: Declare a profile
@Configuration
public class ProductionConfig {
	
	@Inject
	@RepositoryType(StorageType.CLOUD)
	private ItemRepository itemRepository;
	
	@Bean
	public Catalog catalog() {
		CatalogImpl cat = new CatalogImpl(itemRepository);
		return cat;
	}
}